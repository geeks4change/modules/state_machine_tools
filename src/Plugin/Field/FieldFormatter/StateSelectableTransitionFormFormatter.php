<?php

namespace Drupal\state_machine_tools\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\Checkboxes;
use Drupal\state_machine\Plugin\Field\FieldFormatter\StateTransitionFormFormatter;
use Drupal\state_machine\Plugin\Workflow\WorkflowInterface;
use Drupal\state_machine\Plugin\Workflow\WorkflowState;

/**
 * Plugin implementation of the 'state_machine_tools_state_transition_form' formatter.
 *
 * @FieldFormatter(
 *   id = "state_machine_tools_state_transition_form",
 *   label = @Translation("Transition form with selectable transitions"),
 *   field_types = {
 *     "state",
 *   },
 * )
 */
class StateSelectableTransitionFormFormatter extends StateTransitionFormFormatter {
  
  public static function defaultSettings() {
    return parent::defaultSettings() + [
      'allowed_transitions' => NULL,
    ];
  }

  public function settingsSummary() {
    $summary = parent::settingsSummary();
    if ($transitions = $this->getSetting('allowed_transitions')) {
      $summary[] = $this->t('Allowed transitions: @transitions', ['@transitions' => implode(', ', $transitions)]);
    }
    return $summary;
  }

  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $transitionOptions = $this->getTransitionOptions();
    if (isset($transitionOptions)) {
      $form['allowed_transitions'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Allowed transitions'),
        '#options' => $transitionOptions,
        '#default_value' => $this->getSetting('allowed_transitions'),
        '#value_callback' => fn(&$element, $input, FormStateInterface $form_state) => array_values(Checkboxes::valueCallback($element, $input, $form_state)),
      ];
    }
    else {
      $form['allowed_transitions'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Allowed transitions'),
        '#description' => $this->t('The workflow of this field is determined dynamically. Enter one transition ID per line.'),
        '#default_value' => implode("\n", $this->getSetting('allowed_transitions')),
        '#value_callback' => fn(&$element, $input, FormStateInterface $form_state) => preg_split('{\r?\n}u', Element\Textarea::valueCallback($element, $input, $form_state)),
      ];
    }
    return $form;
  }

  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    $formTransitions =& $elements[0]['actions'];
    $allowedTransitionIds = $this->getSetting('allowed_transitions');
    foreach (Element::children($formTransitions) as $transitionId) {
      if (is_array($allowedTransitionIds) && !in_array($transitionId, $allowedTransitionIds)) {
        $formTransitions[$transitionId]['#access'] = FALSE;
      }
    }
    return $elements;
  }


  /**
   * Get transition options if known.
   */
  protected function getTransitionOptions(): ?array {
    $workflow = $this->getWorkflow();
    if (!$workflow) {
      // The workflow is not known yet, might be a callback.
      return NULL;
    }
    /** @noinspection PhpUnnecessaryLocalVariableInspection */
    $transitionLabels = array_map(function (WorkflowState $state) {
      return $state->getLabel();
    }, $workflow->getTransitions());

    return $transitionLabels;
  }

  /**
   * Get workflow if known.
   *
   * @see \Drupal\state_machine\Plugin\Field\FieldType\StateItem::getWorkflow
   */
  public function getWorkflow(): ?WorkflowInterface {
    $workflow_id = $this->fieldDefinition->getSetting('workflow');
    if (empty($workflow_id)) {
      // Field might have dynamic workflow_callback.
      return NULL;
    }
    $workflow_manager = \Drupal::service('plugin.manager.workflow');
    return $workflow_manager->createInstance($workflow_id);
  }

}
